package cn.edu.dgut.sw.security.oauth2.weixin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeiXinOAuth2DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeiXinOAuth2DemoApplication.class, args);
	}
}
