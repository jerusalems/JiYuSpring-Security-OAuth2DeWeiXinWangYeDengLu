/**
 * 
 */
/**
 * @author dgsai
 *
 */
package cn.edu.dgut.sw.security.oauth2.weixin.controller;

import java.security.Principal;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeiXinOauth2Controller{
	
	@RequestMapping("/user")
	public Principal user(Principal principal) {
		return principal;
	}
	
}