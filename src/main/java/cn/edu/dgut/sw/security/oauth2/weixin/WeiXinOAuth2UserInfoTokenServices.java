package cn.edu.dgut.sw.security.oauth2.weixin;

import java.util.Map;

import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;

/**
 * 微信网页登录用户信息services
 * @author dgsai
 * @date 2017-8-14
 */
public class WeiXinOAuth2UserInfoTokenServices extends UserInfoTokenServices {

	public WeiXinOAuth2UserInfoTokenServices(String userInfoEndpointUrl, String clientId) {
		super(userInfoEndpointUrl, clientId);
		
		// 设置微信登录后拉取的用户信息的主键解释器（获取openid的值）
		setPrincipalExtractor(new PrincipalExtractor() {
			@Override
			public Object extractPrincipal(Map<String, Object> map) {
				if (map.containsKey("openid")) {
					return map.get("openid");
				}
				return null;
			}
		});
		
		// 微信登录后，拉取的用户信息中没有权限的信息。
		// 那么经父类的authoritiesExtractor解释后，默认为ROLE_USER
		// 可以去掉下面方法的注释，自定义的一个权限解释器。
		// setAuthoritiesExtractor(new AuthoritiesExtractor() {
		// @Override
		// public List<GrantedAuthority> extractAuthorities(Map<String, Object> map) {
		// return null;
		// }
		// });
	}

}
