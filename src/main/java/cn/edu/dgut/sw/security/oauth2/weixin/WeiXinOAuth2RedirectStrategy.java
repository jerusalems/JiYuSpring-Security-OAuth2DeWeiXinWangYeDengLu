package cn.edu.dgut.sw.security.oauth2.weixin;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.DefaultRedirectStrategy;

public class WeiXinOAuth2RedirectStrategy extends DefaultRedirectStrategy{

	@Override
	public void sendRedirect(HttpServletRequest request, HttpServletResponse response, String url) throws IOException {
		// 在微信网页授权文档里指出，无论直接打开还是做页面302重定向时候，必须带#wechat_redirect参数。
		super.sendRedirect(request, response, url + "#wechat_redirect");
	}

}
