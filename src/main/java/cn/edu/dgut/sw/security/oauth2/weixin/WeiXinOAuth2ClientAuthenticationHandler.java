package cn.edu.dgut.sw.security.oauth2.weixin;

import org.springframework.http.HttpHeaders;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.auth.DefaultClientAuthenticationHandler;
import org.springframework.util.MultiValueMap;

/**
 * 重写authenticateTokenRequest方法
 * 在请求获取access_token时，把client_id与client_secret改名为appid与secret
 * 
 * @author dgsai
 *
 */
public class WeiXinOAuth2ClientAuthenticationHandler extends DefaultClientAuthenticationHandler {

	@Override
	public void authenticateTokenRequest(OAuth2ProtectedResourceDetails resource, MultiValueMap<String, String> form,
			HttpHeaders headers) {
		
		super.authenticateTokenRequest(resource, form, headers);
		
		String clientId = form.getFirst("client_id");
		String clientSecret = form.getFirst("client_secret");
		form.set("appid", clientId);
		form.set("secret", clientSecret);
		form.remove("client_id");
		form.remove("client_secret");
	}
}
