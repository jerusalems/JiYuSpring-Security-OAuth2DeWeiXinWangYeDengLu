package cn.edu.dgut.sw.security.oauth2.weixin;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

/**
 * 微信网页登录REST控制器
 * 
 * @author dgsai
 *
 */
public class WeiXinOAuth2RestTemplate extends OAuth2RestTemplate {

	/**
	 * 自定义WeiXinOAuth2RestTemplate的构造函数
	 * 
	 * @param resource
	 * @param context
	 */
	public WeiXinOAuth2RestTemplate(OAuth2ProtectedResourceDetails resource, OAuth2ClientContext context) {
		super(resource, context);
		getMessageConverters().add(new WeiXinOAuth2MappingJackson2HttpMessageConverter());
		WeiXinOAuth2AuthorizationCodeAccessTokenProvider weiXinAuthorizationCodeAccessTokenProvider = new WeiXinOAuth2AuthorizationCodeAccessTokenProvider();
		weiXinAuthorizationCodeAccessTokenProvider.setAuthenticationHandler(new WeiXinOAuth2ClientAuthenticationHandler());
		setAccessTokenProvider(weiXinAuthorizationCodeAccessTokenProvider);
	}

	/**
	 * 标准的oauth2协议是没有openid的，但微信网页登录的API是必需的。
	 * 重写appendQueryParameter方法：
	 * 在请求拉取用户信息(scope必需为 snsapi_userinfo)，根据配置文件的设置，在请求url里增加openid。
	   weixin:
	     client:
	       scope:
	       - snsapi_userinfo
	   weixin:
	     resource:
	       userInfoUri: https://api.weixin.qq.com/sns/userinfo?lang=zh_CN&openid=$openid$
	 * 
	 */
	@Override
	protected URI appendQueryParameter(URI uri, OAuth2AccessToken accessToken) {
		
		uri = super.appendQueryParameter(uri, accessToken);
		String url = uri.toString();
		if (url.contains("$openid$")) {
			url.replace("$openid$", (String) accessToken.getAdditionalInformation().get("openid"));
		}
		
		try {
			uri = new URI(url);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return uri;
	}
}
