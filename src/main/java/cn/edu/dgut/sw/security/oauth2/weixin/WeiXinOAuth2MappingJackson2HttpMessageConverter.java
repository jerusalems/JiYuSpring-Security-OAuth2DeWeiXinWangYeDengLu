package cn.edu.dgut.sw.security.oauth2.weixin;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

/**
 * 微信网页登录messageConverter
 * 使MappingJackson2HttpMessageConverter可以转换content-type=text/plain为JSON
 * 
 * @author dgsai
 *
 */
public class WeiXinOAuth2MappingJackson2HttpMessageConverter extends MappingJackson2HttpMessageConverter {

	public WeiXinOAuth2MappingJackson2HttpMessageConverter() {
		List<MediaType> mediaTypes = new ArrayList<>();
		mediaTypes.add(MediaType.TEXT_PLAIN);
		setSupportedMediaTypes(mediaTypes);
	}
}
